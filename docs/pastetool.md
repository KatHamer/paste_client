# Pastetool

Pastetool is a simple tool to interact with [paste](https://gitlab.com/KatHamer/paste/). By default it's configured to work with https://paste.katelyn.dev, but can be used with any paste endpoint. 

# Usage

- create-paste   Get a paste
- delete-paste   Delete a paste
- edit-paste     Edit a paste
- get-paste      Get a paste
- request-token  Request a token

Run `pastetool --help` for a full list of commands.

# Examples

- Get a token: `pastetool request-token -u <user> -p <pass>`
- Create a paste: `pastetool create-paste`
- Get a paste: `pastetool get-paste <paste_id>`
- Pipe a script into a paste: `./script.sh | pastetool create-paste`