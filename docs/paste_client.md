# paste_client

paste_client is a Python module that allows you to interact with the [paste](https://gitlab.com/KatHamer/paste/) API in a Pythonic manner. 

# PasteClient class

The PasteClient class is the main interface of this module, it provides the following functions:

- `get_token`
- `get_paste`
- `create_paste`
- `edit_paste`
- `delete_paste`

which correspond to the appropriate actions in the API.

`__init__` has two potential arguments, `endpoint_url` and `access_token`, the latter of which can be ommited from instanciation and set later once an access token has been obtained.

# Exceptions

paste_client provides two exception types, `APIError` and `AuthError`.

### APIError

The APIError exception is a generic exception used to indicate any error returned by the API, it should be instanciated with the error response as the only parameter.

Members:
`response` - The error response
`status` - HTTP status code
`message` - Set if response json contains a `message` field

### AuthError

The AuthError exception is more specific and is used to indicate authentication errors with the API. It should also be raised if you lack the credentials to call a request, i.e. if the access token has not been set.

# Basic usage

### Import the module and initialise the PasteClient class for the first time

```python
>>> from paste_client import PasteClient
>>>
>>> client = PasteClient('https://paste.katelyn.dev')
```

### Request a token from the API to perform authenticated calls

```python
>>> ACCESS_TOKEN = client.get_token('<HTPASSWD_USER>', '<HTPASSWD_PASS>')
```

### Create a paste

```python
>>> client.create_paste("Hello, World!" public=True)
```

