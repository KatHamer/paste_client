"""
paste_client - cli.py
Provides a command-line interface for paste.katelyn.dev using client.py
By Katelyn Hamer
"""

from .client import PasteClient, APIError, AuthError
import click
import os
import sys
import logging

logger = logging.getLogger(__name__)

# Set API creds + endpoint URL from env vars
api_access_token = os.getenv("PASTE_ACCESS_TOKEN")
api_endpoint_url = os.getenv(
    "PASTE_ENDPOINT_URL") or "https://paste.katelyn.dev"

if not api_access_token:
    if sys.argv[1] != "request-token":  # Supress token warnings if invoking request-token
        click.secho(
            f"Warning: PASTE_ACCESS_TOKEN not set, request a token with 'paste request-token'", fg="yellow", err=True)

paste_client = PasteClient(api_endpoint_url, api_access_token)


@click.group()
def cli():
    ...


@cli.command(name="request-token")
@click.option('-u', '--user', help='Your HTTP basic auth username', required=True)
@click.option('-p', '--password', help='Your HTTP basic auth username', required=True)
def request_token(user: str, password: str):
    """Request a token"""
    try:
        token = paste_client.get_token(user, password)
        click.echo(f"Your API token is: {token}")
        click.echo(
            "Please keep this token safe as it allows access to your pastes")
        click.echo(
            "To use it in subsequent commands, set the PASTE_ACCESS_TOKEN environment variable")
    except APIError as api_error:
        if api_error.message:
            click.secho(str(api_error), fg="red", err=True)
        else:
            click.secho(
                f"Error: API returned non-200 response {api_error.status}", fg="red", err=True)
        sys.exit(1)
    except AuthError as auth_error:
        click.secho(str(auth_error), fg="red", err=True)
        sys.exit(1)


@cli.command(name="get-paste")
@click.argument('paste_id')
def get_paste(paste_id: str):
    """Get a paste"""
    try:
        paste = paste_client.get_paste(paste_id)
        click.echo(f"Paste by {paste['author']} - {paste['filename']}")
        click.echo(f"{paste['content']}")
    except APIError as api_error:
        if api_error.message:
            click.secho(str(api_error), fg="red", err=True)
            sys.exit(1)
        else:
            click.secho(
                f"Error: API returned non-200 response {api_error.status}", fg="red", err=True)
            sys.exit(1)
    except AuthError as auth_error:
        click.secho(str(auth_error), fg="red", err=True)
        sys.exit(1)


@cli.command(name="create-paste")
@click.option('--content', help='Content to paste', required=False)
@click.option('--public/--private', help='Whether paste should be public or not', default=False)
@click.option('--filename', help='Paste filename', required=False)
def create_paste(content: str, filename: str, public: bool):
    """Get a paste"""
    # First we check if content has been populated from click, if not we attempt to populate it from stdin
    if not content:
        if not click.get_text_stream('stdin').isatty():
            content = click.get_text_stream('stdin').read().strip()
        else:
            click.secho(
                "Error: --content option not provided and nothing piped in", fg="red", err=True)
            sys.exit(1)

    try:
        paste_id, paste_url = paste_client.create_paste(
            content, filename, public)
        click.echo(f"Created paste {paste_id}. Paste url: {paste_url}")
    except APIError as api_error:
        if api_error.message:
            click.secho(str(api_error), fg="red", err=True)
        else:
            click.secho(
                f"Error: API returned non-200 response {api_error.status}", fg="red", err=True)
    except AuthError as auth_error:
        click.secho(str(auth_error), fg="red", err=True)


@cli.command(name="delete-paste")
@click.argument('paste_id')
def delete_paste(paste_id: str):
    """Delete a paste"""
    try:
        paste_client.delete_paste(paste_id)
        click.echo(f"Successfully deleted paste {paste_id}")
    except APIError as api_error:
        if api_error.message:
            click.secho(str(api_error), fg="red", err=True)
        else:
            click.secho(
                f"Error: API returned non-200 response {api_error.status}", fg="red", err=True)
    except AuthError as auth_error:
        click.secho(str(auth_error), fg="red", err=True)


@cli.command(name="edit-paste")
@click.option('--content', help='Content to paste', required=False)
@click.argument('paste_id')
def edit_paste(paste_id: str, content: str):
    """Edit a paste"""

    # First we check if content has been populated from click, if not we attempt to populate it from stdin
    if not content:
        if not click.get_text_stream('stdin').isatty():
            content = click.get_text_stream('stdin').read().strip()
        else:
            click.secho(
                "Error: --content option not provided and nothing piped in", fg="red", err=True)
            sys.exit(1)

    try:
        paste_client.edit_paste(paste_id, content)
        click.echo(f"Successfully edited paste {paste_id}")
    except APIError as api_error:
        if api_error.message:
            click.secho(str(api_error), fg="red", err=True)
        else:
            click.secho(
                f"Error: API returned non-200 response {api_error.status}", fg="red", err=True)
    except AuthError as auth_error:
        click.secho(str(auth_error), fg="red", err=True)
