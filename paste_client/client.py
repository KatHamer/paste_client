"""
paste_client - client.py
Provides the PasteClient class for interacting with the paste.katelyn.dev API
By Katelyn Hamer
"""

import requests
from requests.auth import HTTPBasicAuth
import logging
import json
from typing import Tuple

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
stream_handler = logging.StreamHandler()
formatter = logging.Formatter(logging.BASIC_FORMAT)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


class APIError(Exception):
    """Generic exception for API errors"""

    def __init__(self, response):
        self.response = response
        self.status = response.status_code

        try:
            self.message = response.json().get('message')
        except json.decoder.JSONDecodeError:  # Occurs when we get a non-200 response from the web server or Flask rather than the API
            self.message = ""  # In this case we just set self.message to an empty value

    def __str__(self):
        if self.message:
            return self.message
        else:
            return f"status {self.status}"


class AuthError(Exception):
    """Exception for authorization errors, either when the API returns 401 or we lack the credentials to make a call"""

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class PasteClient:
    def __init__(self, endpoint_url: str, access_token: str = ""):
        # Set API creds + endpoint URL from env vars
        self.api_access_token = access_token
        self.api_endpoint_url = endpoint_url

    def get_token(self, htpasswd_user: str, htpasswd_pass: str) -> str:
        """
        Get an access token from HTTP basic auth credentials by calling /token/get

        Params:
            htpasswd_user - HTTP basic auth username
            htpasswd_pass - HTTP basic auth password

        Returns: an access token
        """
        response = requests.get(
            f"{self.api_endpoint_url}/token/get", auth=HTTPBasicAuth(htpasswd_user, htpasswd_pass))
        if response.ok:
            # We use [] here instead of .get so that the AttributeError will fall to Python
            token = response.json()['token']
            return token
        else:
            logger.error("/token/get returned non-200 response!")
            raise APIError(response)

    def get_paste(self, paste_id: str) -> dict:
        """
        Get a paste by calling /paste/get

        Params:
            paste_id - ID of paste

        Returns: a paste dict
        """
        response = requests.get(
            f"{self.api_endpoint_url}/paste/get", json={"id": paste_id}, headers={"Authorization": f"token {self.api_access_token}"})
        if response.ok:
            return response.json()
        else:
            logger.error("/paste/get returned non-200 response!")
            raise APIError(response)

    def create_paste(self, content: str, filename: str = None, public: bool = False) -> Tuple[str, str]:
        """
        Create a paste by calling /paste/new

        Params:
            content - Paste content
            filename - Paste filename
            public - Whether paste is public or not

        Returns: paste id, url to paste
        """
        response = requests.post(f"{self.api_endpoint_url}/paste/new", json={
                                "content": content, "filename": filename, "public": public}, headers={"Authorization": f"token {self.api_access_token}"})
        if response.ok:
            paste_id = response.json()['id']
            paste_url = response.json()['url']
            return paste_id, paste_url
        else:
            logger.error("/paste/new returned non-200 response!")
            raise APIError(response)

    def delete_paste(self, paste_id: str):
        """
        Delete a paste by calling /paste/delete

        Params:
            paste_id - Paste to delete
        """
        response = requests.delete(f"{self.api_endpoint_url}/paste/delete", json={
                                   'id': paste_id}, headers={"Authorization": f"token {self.api_access_token}"})
        if response.ok:
            return
        else:
            logger.error("/paste/delete returned non-200 response!")
            raise APIError(response)

    def edit_paste(self, paste_id: str, content: str):
        """
        Create a paste by calling /paste/new

        Params:
            content - Paste content
            filename - Paste filename
            public - Whether paste is public or not

        Returns: paste id, url to paste
        """
        response = requests.put(f"{self.api_endpoint_url}/paste/edit", json={'id': paste_id,
                                'content': content}, headers={"Authorization": f"token {self.api_access_token}"})
        if response.ok:
            return
        else:
            logger.error("/paste/edit returned non-200 response!")
            raise APIError(response)
