# paste_client

This repo provides both a Python module (paste_client) and cli utility (paste_tool) for interacting with [paste](https://gitlab.com/KatHamer/paste/).

For **pastetool**'s documentation, see [pastetool.md](docs/pastetool.md)  
For **paste_client**'s documentation, see [paste_client.md](docs/paste_client.md)




